<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductBooking;
use App\ProductVariation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Validation\Rule;

class ProductCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('templates.product_cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=Validator::make($request->all(), [
            'quantity' => 'required|numeric',
            'size' => 'required',
            'product_id' => 'required|exists:products,id',
            'variation_id' => 'required|exists:product_variations,id',
        ]);

        if ($validation->fails()){
            header('HTTP/1.1 400 Bad Request');
            exit('Invalid Request');
        }

        $product=Product::findOrFail($request->product_id);
        $product_variation=ProductVariation::findOrFail($request->variation_id);

        if ($request->quantity > $product_variation->sku){
            header('HTTP/1.1 400 Bad Request');
            exit('Requested quantity is not available');
        }

        $min_quantity=$product_variation->min_quantity;
        $regular_price=$product_variation->regular_price;
        $unit_price=$product_variation->discount_price;
        if ((Auth::check()) && (Auth::user()->user_type == 'wholesaler')){
            $min_quantity=$product_variation->min_wholesale_quantity;
            $regular_price=$product_variation->wholesale_price;
            $unit_price=$product_variation->discount_wholesale_price;
        }

        if ($request->quantity < $min_quantity){
            header('HTTP/1.1 400 Bad Request');
            exit('Product quality less than minimum');
        }

        $session_data=session('cart_data');

        $new_cart=[
            'id'=>$request->product_id,
            'title'=>$product->title,
            'product_code'=>$product->product_code,
            'product_image'=>$product->default_image,
            'review'=>$product->review['total_review'],
            'rating'=>$product->review['avg_rating'],
            'color'=>$product_variation->color,
            'product_size'=>$request->size,
            'regular_price'=>$regular_price,
            'unit_price'=>$unit_price,
            'quantity'=>$request->quantity
        ];

        if (!is_null($session_data)){
            $cart_data=json_decode($session_data,true);
            array_push($cart_data,$new_cart);
            session(['cart_data' => json_encode($cart_data)]);
        }
        else{
            session(['cart_data' => json_encode([$new_cart])]);
        }

//        $user_id=null;
//        if (Auth::check()){
//            $user_id=Auth::id();
//        }

//        ProductBooking::create(['user_id'=>$user_id, 'product_id'=>$request->product_id, 'product_variation_id'=>$request->variation_id, 'booking_time'=>Carbon::now(), 'quantity'=>$request->quantity]);

        return response()->json(['success'=>true,'name'=>$product->title,'image'=>'/storage/'.$product->default_image]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session_data=session('cart_data');
        $cart_data=json_decode($session_data,true);
        unset($cart_data[$id]);
        $cart_data=array_values($cart_data);
        session(['cart_data' => json_encode($cart_data)]);
        return redirect()->back();
    }

    public function product_checkout(){
        return view('templates.product_checkout');
    }

    public function order_product(){

    }

    public function get_cart_data(){
        $session_data=session('cart_data');
        $coupon_data=session('coupons_data');
        $product_charts=[];
        $discount_amount=0;
        if (!is_null($session_data)){
            $product_charts=json_decode($session_data,true);
        }
        if (!is_null($coupon_data)){
            $all_coupon_data=json_decode($coupon_data,true);
            foreach ($all_coupon_data as $coupon_data){
                $discount_amount+=$coupon_data['discount_amount'];
            }
        }
        return response()->json(['product_charts'=>$product_charts, 'discount_amount'=>$discount_amount, 'total_items'=>count($product_charts)]);
    }

    public function update_cart_data(Request $request){
        session(['cart_data' => $request->cart_data]);
    }
}
