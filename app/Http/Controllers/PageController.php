<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages=Page::all();
        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_templates=PageTemplate::all();
        return view('admin.pages.create', compact('page_templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug='home')
    {
        $page=Page::with('template')->where('slug',$slug)->first();
        abort_unless(!empty($page), 404);
        return view('templates.'.$page->template['template_name'], compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page=Page::with('template')->find($id);
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator=Validator::make($request->all(), [
            'page_title' => [
                'required',
                Rule::unique('pages')->ignore($id),
            ],
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $page=Page::with('template')->find($id);

        if ($page->template['template_name'] == 'about'){
            $allContents=$request->contents;
            $section_contents=[];
            $sl=0;
            foreach ($allContents['section'] as $index=>$section){
                if (isset($section['section_image'])){

                    $path=$section['section_image']->store('images');

                    $image = Image::make(Storage::get($path))->resize(490, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->encode();

                    Storage::put($path, $image);
                    $section_contents['section'][$sl]['section_image']=$path;
                }
                else{
                    if (isset($section['old_section_image'])){
                        $section_contents['section'][$sl]['section_image']=$section['old_section_image'];
                    }
                    else{
                        $section_contents['section'][$sl]['section_image']='';
                    }
                }

                $section_contents['section'][$sl]['title']=$section['title'];
                $section_contents['section'][$sl]['details']=$section['details'];
                $section_contents['section'][$sl]['layout']=$section['layout'];
                $section_contents['section'][$sl]['bg_color']=$section['bg_color'];
                $sl++;
            }

            $page->contents=$section_contents;

        }

        else{
            $page->contents=$request->contents;
        }


        $page->page_title=$request->page_title;
        $page->slug=$request->slug;
        $page->save();
        flash('Page Updated Successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);
        flash('Page Deleted Successfully');
        return redirect()->action('PageController@index');
    }

    public function removeImage(Request $request){
        $page=Page::find($request->page_id);
        $page_content=$page->contents;
        Storage::delete($page_content['section'][$request->image_id]['section_image']);
        $page_content['section'][$request->image_id]['section_image']='';
        $page->contents=$page_content;
        $page->save();
    }

    public function removeSection(Request $request){
        $page=Page::with('template')->find($request->page_id);
        $page_content=$page->contents;
        Storage::delete($page_content['section'][$request->section_id]['section_image']);
        array_splice($page_content['section'], $request->section_id, 1);
        $page->contents=$page_content;
        $page->save();
    }

    public function loadData($id){
        $page=Page::with('template')->find($id);
        $page_template=$page->template['template_name'];
        return view('admin.template.'.$page_template, compact('page'));
    }
}
