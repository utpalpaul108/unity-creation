<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\ProductCategory;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Arr;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog_posts=Blog::all();
        return view('admin.blog.index',compact('blog_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blog_tags=Tag::all()->toArray();
//        $tags=Arr::pluck($blog_tags, 'name');
        $tags=json_encode(['test01','test02','test03']);
        $product_categories=ProductCategory::all();
        return view('admin.blog.create',compact('product_categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tags=$request->tags;
        $tagsId=[];
        foreach ($tags as $index=>$tag){
            $tag=Tag::firstOrCreate(['name' => $tag]);
            $tagsId[$index]=$tag->id;
        }
        $allData=$request->all();
        $allData['created_by']=Auth::id();
        if ($request->hasFile('feature_image')){
            $path=$request->file('feature_image')->store('images/blog');
            $medium_path=$request->file('feature_image')->store('medium/images/blog');
            $small_path=$request->file('feature_image')->store('small/images/blog');
            $image = Image::make(Storage::get($path))->fit(1110, 600)->encode();
            $medium_image = Image::make(Storage::get($path))->fit(540, 540)->encode();
            $small_image = Image::make(Storage::get($path))->fit(255, 306)->encode();
            Storage::put($path, $image);
            Storage::put($medium_path, $medium_image);
            Storage::put($small_path, $small_image);
            $allData['feature_image']=$path;
        }

        $blog=Blog::create($allData);
        $blog->tags()->sync($tagsId);
        flash('Blog post created successfully');
        return redirect()->action('Admin\BlogController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_categories=ProductCategory::all();
        $blog_post=Blog::find($id);
        return view('admin.blog.edit',compact('blog_post','product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog_post=Blog::find($id);
        if ($request->hasFile('feature_image')){
            Storage::delete($blog_post->feature_image);
            Storage::delete('/medium/'.$blog_post->feature_image);
            Storage::delete('/small/'.$blog_post->feature_image);
            $path=$request->file('feature_image')->store('images/blog');
            $medium_path=$request->file('feature_image')->store('medium/images/blog');
            $small_path=$request->file('feature_image')->store('small/images/blog');
            $image = Image::make(Storage::get($path))->fit(1110, 600)->encode();
            $medium_image = Image::make(Storage::get($path))->fit(540, 540)->encode();
            $small_image = Image::make(Storage::get($path))->fit(255, 306)->encode();
            Storage::put($path, $image);
            Storage::put($medium_path, $medium_image);
            Storage::put($small_path, $small_image);
            $blog_post->feature_image=$path;
        }

        $blog_post->title=$request->title;
        $blog_post->category_id=$request->category_id;
        $blog_post->short_description=$request->short_description;
        $blog_post->details=$request->details;
        $blog_post->updated_by=Auth::id();
        $blog_post->save();
        flash('Blog post updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog_post=Blog::find($id);
        Storage::delete($blog_post->feature_image);
        Blog::destroy($id);
        flash('Blog post deleted successfully');
        return redirect()->action('Admin\BlogController@index');
    }
}
