<?php

namespace App\Http\Controllers\Admin;

use App\MenuItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allData=$request->all();
        if ($request->hasFile('menu_image')){
            $path=$request->file('menu_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(110, 235)->encode();
            Storage::put($path, $image);
            $allData['menu_image']=$path;
        }
        $allData['menu_id']=1;
        $allData['order']=Null;
        MenuItem::create($allData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $menu=MenuItem::find($request->id);
        $menu->title=$request->title;
//        $menu->url=$request->url;
        $menu->target=$request->target;
        if ($request->hasFile('menu_image')){
            Storage::delete($menu->menu_image);
            $path=$request->file('menu_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(110, 235)->encode();
            Storage::put($path, $image);
            $menu->menu_image=$path;
        }
        $menu->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MenuItem::destroy($id);
    }
}
