<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductCategory;
use App\ProductVariation;
use App\ProductWiseCategory;
use App\ProductWiseSubcategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::all();
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories=ProductCategory::all();
        return view('admin.product.create',compact('product_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'details' => 'required',
            'average_price' => 'required',
            'is_trend_product' => 'required',
        ]);

        $allData=$request->all();
        if ($request->hasFile('default_image')){
            $path=$request->file('default_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(1920, 800)->encode();
            Storage::put($path, $image);
            $allData['default_image']=$path;
        }

        $product=Product::create($allData);
        $product->product_code='PD'.$product->id.date('dmy');
        $product->save();

        $product_variations = $request->product_variation;

        foreach ($product_variations as $variation){
            $product_variation= new ProductVariation();
            $product_variation->product_id=$product->id;
            $product_variation->color=$variation['color'] ?? '';
            $product_variation->sizes=$variation['sizes'] ?? [];
            $product_variation->regular_price=$variation['regular_price'] ?? '';
            $product_variation->discount_price=$variation['discount_price'] ?? '';
            $product_variation->min_quantity=$variation['min_quantity'] ?? '';
            $product_variation->wholesale_price=$variation['wholesale_price'] ?? '';
            $product_variation->discount_wholesale_price=$variation['discount_wholesale_price'] ?? '';
            $product_variation->min_wholesale_quantity=$variation['min_wholesale_quantity'] ?? '';
            $product_variation->sku=$variation['sku'] ?? '';
            $product_variation->product_images=$variation['product_images'] ?? '';
            $product_variation->save();
        }

        $product_categories=$request->product_category;
        foreach ($product_categories as $category){
            $product_wise_category=new ProductWiseCategory();
            $product_wise_category->product_id=$product->id;
            $product_wise_category->category_id=$category;
            $product_wise_category->save();
        }

        $product_subcategories=$request->product_subcategory;

        foreach ($product_subcategories as $subcategory){
            $product_wise_subcategory=new ProductWiseSubcategories();
            $product_wise_subcategory->product_id=$product->id;
            $product_wise_subcategory->subcategory_id=$subcategory;
            $product_wise_subcategory->save();
        }

        flash('New product added successfully');
        return redirect()->action('Admin\ProductController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::findOrFail($id);
        $product_categories=ProductCategory::all();
        $product_wise_categories=ProductWiseCategory::where('product_id',$id)->get()->pluck('category_id')->all();
        $product_wise_subcategories=ProductWiseSubcategories::where('product_id',$id)->get()->pluck('subcategory_id')->all();
        return view('admin.product.edit',compact('product','product_categories','product_wise_categories','product_wise_subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'details' => 'required',
            'average_price' => 'required',
        ]);

        $product=Product::find($id);
        if ($request->hasFile('default_image')){
            Storage::delete($product->default_image);
            $path=$request->file('default_image')->store('images');
            $image = Image::make(Storage::get($path))->fit(537, 647)->encode();
            Storage::put($path, $image);
            $product->default_image=$path;
        }

        $product->title=$request->title;
        $product->details=$request->details;
        $product->average_price=$request->average_price;
        $product->is_trend_product=$request->is_trend_product;
        $product->save();
        ProductVariation::where('product_id',$id)->delete();
        $product_variations = $request->product_variation;

        foreach ($product_variations as $variation){
            $product_variation= new ProductVariation();
            $product_variation->product_id=$product->id;
            $product_variation->color=$variation['color'] ?? '';
            $product_variation->sizes=$variation['sizes'] ?? [];
            $product_variation->regular_price=$variation['regular_price'] ?? '';
            $product_variation->discount_price=$variation['discount_price'] ?? '';
            $product_variation->min_quantity=$variation['min_quantity'] ?? '';
            $product_variation->wholesale_price=$variation['wholesale_price'] ?? '';
            $product_variation->discount_wholesale_price=$variation['discount_wholesale_price'] ?? '';
            $product_variation->min_wholesale_quantity=$variation['min_wholesale_quantity'] ?? '';
            $product_variation->sku=$variation['sku'] ?? '';
            $product_variation->product_images=$variation['product_images'] ?? '';
            $product_variation->save();
        }

        ProductWiseCategory::where('product_id',$id)->delete();
        $product_categories=$request->product_category;
        foreach ($product_categories as $category){
            $product_wise_category=new ProductWiseCategory();
            $product_wise_category->product_id=$product->id;
            $product_wise_category->category_id=$category;
            $product_wise_category->save();
        }

        ProductWiseSubcategories::where('product_id',$id)->delete();
        $product_subcategories=$request->product_subcategory;
        foreach ($product_subcategories as $subcategory){
            $product_wise_subcategory=new ProductWiseSubcategories();
            $product_wise_subcategory->product_id=$product->id;
            $product_wise_subcategory->subcategory_id=$subcategory;
            $product_wise_subcategory->save();
        }

        flash('Product updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        flash('Product deleted successfully');
        return redirect()->action('Admin\ProductController@index');
    }
}
