<?php

namespace App\Http\Controllers\Admin;

use App\ProductCategory;
use App\ProductSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_subcategories=ProductSubcategory::with('category')->get();
        return view('admin.product.subcategory.index',compact('product_subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories=ProductCategory::all();
        return view('admin.product.subcategory.create',compact('product_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ProductSubcategory::create($request->all());
        flash('Product Subcategory added successfully');
        return redirect()->action('Admin\ProductSubcategoryController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_categories=ProductCategory::all();
        $product_subcategory=ProductSubcategory::find($id);
        return view('admin.product.subcategory.edit',compact('product_subcategory','product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product_subcategory=ProductSubcategory::find($id);
        $product_subcategory->name=$request->name;
        $product_subcategory->category_id=$request->category_id;
        $product_subcategory->save();
        flash('Product Subcategory updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductSubcategory::destroy($id);
        flash('Product Subcategory deleted successfully');
        return redirect()->action('Admin\ProductSubcategoryController@index');
    }
}
