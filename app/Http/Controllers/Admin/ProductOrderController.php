<?php

namespace App\Http\Controllers\Admin;

use App\OrderDetails;
use App\ProductOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_orders=ProductOrder::where('order_status','processing')->get();
        return view('admin.product_order.index',compact('product_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_order=ProductOrder::findOrFail($id);
        $order_items=OrderDetails::with('product')->where('order_id',$id)->get();
        return view('admin.product_order.show',compact('product_order','order_items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductOrder::destroy($id);
        flash('Order deleted successfully');
        return redirect()->action('Admin/ProductOrderController@index');
    }

    public function delivered($id){
        $product_order=ProductOrder::findOrFail($id);
        $product_order->order_status='delivered';
        $product_order->delivery_date=Carbon::now();
        $product_order->save();
        $order_details=OrderDetails::where('order_id',$id)->firstOrFail();
        $order_details->status='delivered';
        $order_details->save();
        flash('Product delivered successfully');
        return redirect()->action('Admin\ProductOrderController@index');
    }

    public function delivered_products(){
        $product_delivered=ProductOrder::where('order_status','delivered')->get();
        return view('admin.product_order.delivered',compact('product_delivered'));
    }


}
