<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Validation\Rule;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons=Coupon::all();
        return view('admin.coupon.index',compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'coupon_code' => 'required|unique:coupons',
            'discount_type' => 'required',
            'amount' => 'required',
            'expiry_date' => 'required',
            'limit_per_coupon' => 'required',
            'limit_per_user' => 'required',
        ]);

        Coupon::create($request->all());
        flash('Coupon created successfully');
        return redirect()->action('Admin\CouponController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon=Coupon::find($id);
        return view('admin.coupon.edit',compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation=Validator::make($request->all(), [
            'coupon_code' => [
                'required',
                Rule::unique('coupons')->ignore($id),
            ],
            'discount_type' => 'required',
            'amount' => 'required',
            'expiry_date' => 'required',
            'limit_per_coupon' => 'required',
            'limit_per_user' => 'required',
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $coupon=Coupon::find($id);
        $coupon->coupon_code=$request->coupon_code;
        $coupon->description=$request->description;
        $coupon->discount_type=$request->discount_type;
        $coupon->amount=$request->amount;
        $coupon->expiry_date=$request->expiry_date;
        $coupon->limit_per_coupon=$request->limit_per_coupon;
        $coupon->limit_per_user=$request->limit_per_user;
        $coupon->save();
        flash('Coupon updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::destroy($id);
        flash('Coupon deleted successfully');
        return redirect()->back();
    }
}
