<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Validation\Rule;


class ProductReviewController extends Controller
{
    public function create($product_id){
        $product=Product::find($product_id);
        abort_unless(!empty($product), 404);
        $product_review=ProductReview::where([['product_id',$product_id],['user_id',Auth::id()]])->first();
        return view('product.review',compact('product','product_review'));
    }

    public function store(Request $request){
        $validation=Validator::make($request->all(), [
            'rating' => 'required',
            'product_id' => 'exists:products,id'
        ]);

        if ($validation->fails()){
            return redirect()->back()->withErrors($validation);
        }

        $allData=$request->all();
        $allData['user_id']=Auth::id();
        ProductReview::updateOrCreate(['user_id'=>Auth::id(), 'product_id'=>$request->product_id],['rating'=>$request->rating, 'review'=>$request->review]);
        session()->flash('success_message','Review provided successfully');
        return redirect()->back();
    }
}
