<?php

use App\SiteSetting;
use App\Product;

if (! function_exists('setting')) {
        function setting($key = null)
        {
            $settings=SiteSetting::all()->toArray();
            if (is_array($settings)){
                $allData=array_column($settings,'value','key');
                if (isset($allData[$key])){
                    return $allData[$key];
                }
                else{
                    return null;
                }
            }
            else{
                return null;
            }

        }
    }

if (! function_exists('product_details')) {
    function product_details($id)
    {
        $product=Product::find($id);
        return $product;
    }
}
