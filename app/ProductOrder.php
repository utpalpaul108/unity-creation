<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOrder extends Model
{
    use SoftDeletes;
    protected $fillable=['order_id','order_by','billing_address','shipping_address','order_status'];

    protected $casts=['billing_address'=>'array', 'shipping_address'=>'array'];

    public function ordered_by(){
        return $this->belongsTo('App\User','order_by');
    }

    public function payment(){
        return $this->hasOne('App\OrderPayment','order_id');
    }

    public function order_details(){
        return $this->hasMany('App\OrderDetails','order_id');
    }

    protected $dates=['delivery_date'];
}
