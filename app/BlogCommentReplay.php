<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCommentReplay extends Model
{
    protected $fillable=['comment_id','user_id','replay'];

    public function created_by(){
        return $this->belongsTo('App\User','user_id');
    }
}
