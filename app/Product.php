<?php

namespace App;

use App\Traits\Shareable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use SoftDeletes,Shareable;
    protected $fillable=['title','product_code','details','average_price','default_image','is_trend_product'];

    protected $shareOptions = [
        'columns' => [
            'title' => 'title'
        ],
        'url' => 'url'
    ];

    public function getUrlAttribute()
    {
        return route('products.show', $this->id);
    }

    public function reviews(){
        return $this->hasMany('App\ProductReview','product_id');
    }

    public function getReviewAttribute() {
        $total_reviews=$this->reviews()->count();

        if ($total_reviews<=0){
            return ['total_review'=>0, 'avg_rating'=>0];
        }
        else{
            $rating=$this->reviews()->select(DB::raw("SUM(rating) as total_rating"))->first();
            $total_rating=$rating->total_rating;
            $actual_rating=$total_rating/$total_reviews;
            return ['total_review'=>$total_reviews, 'avg_rating'=>$actual_rating];
        }
    }

    public function product_variations(){
        return $this->hasMany('App\ProductVariation','product_id');
    }
}
