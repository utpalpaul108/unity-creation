<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $fillable=['order_id','product_id','product_quantity','product_size','product_color','coupon_code','regular_price','discount_price','total_price','status'];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function order(){
        return $this->belongsTo('App\ProductOrder','order_id');
    }
}
