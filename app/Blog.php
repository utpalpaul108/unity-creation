<?php

namespace App;

use App\Traits\Shareable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes,Shareable;
    protected $fillable =['title','category_id','feature_image','short_description','details','created_by'];

    public function post_created(){
        return $this->belongsTo('App\User','created_by');
    }

    public function category(){
        return $this->belongsTo('App\ProductCategory','category_id');
    }

    protected $shareOptions = [
        'columns' => [
            'title' => 'title'
        ],
        'url' => 'url'
    ];

    public function getUrlAttribute()
    {
        return route('blog.show', $this->id);
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}
