<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderGroup extends Model
{
    protected $fillable=['group_name'];
}
