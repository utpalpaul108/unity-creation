<!-- MODAL-STARTS-HERE -->
<!-- ADS-MODAL-STARTS-HERE -->
<div class="modal adsModal" id="AdsModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content  h-100">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="container-fluid  h-100 jumbotron">
                <div class="row justify-content-center align-self-center m-0">
                    <h3 class="text-uppercase">New to unity cration</h3>
                    <p class="text-uppercase"><strong>Join us today and get the offer. Happy shopping.</strong></p><br>
                    <div class="card text-center p-0">
{{--                        <img src="/images/coupon.png" alt="">--}}
                        <strong class="text-uppercase">{{ json_decode(setting('special_offer'),true)['title'] }}</strong><br>
                        <span v-text="coupon_response"></span>
                        <form class="d-flex coupon-form" action="{{ action('CouponController@usage') }}" method="post">
                            @csrf
                            <input type="hidden" class="form-control" name="coupon_code" value="{{ json_decode(setting('special_offer'),true)['coupon_code'] }}" placeholder="ENTER COUPON CODE" required>
                            <button v-if="coupon_code" @click="addCoupon('{{ json_decode(setting('special_offer'),true)['coupon_code'] }}')" class="btn btn-coupon text-uppercase" type="button">Get your coupon</button><img src="/images/ajax-loader.gif" style="margin-top: 20px; margin-left: 10px" v-if="coupon_loading">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ADS-MODAL-ENDS-HERE -->
<!-- ADS-MODAL-STARTS-HERE -->
<div class="modal fade auth-modal no-guest-checkout" id="auth-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-body">
                <div class="content-block auth login js-login-register-flow">
                    <ul class="nav nav-pills nav-justified login-tabs no-guest-checkout">
                        <li class="login-tabs__login active"><a data-toggle="tab" href="#login" class="active show">Log in</a></li>
                        <li class="login-tabs__sign-up"><a data-toggle="tab" href="#register">Sign up</a></li>
                    </ul>
                    <div class="login-register-content-block tab-content">
                        <div id="login" class="tab-pane active show">
                            <form action="{{ action('UserController@user_authenticate') }}" method="post" role="form" novalidate="novalidate" class="login">
                                @csrf
                                <div class="form-group login__email">
                                    <label class="control-label sr-only required" for="customer_login_email"> Email
                                    </label>
                                    <input id="customer_login_email" name="email" required="required" class="form-control input-lg" placeholder="Email" type="email">
                                    <span class="input-helper">
										<svg class="next-icon next-icon--color-slate-lighter next-icon--size-20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M460.6 147.3L353 256.9c-.8.8-.8 2 0 2.8l75.3 80.2c5.1 5.1 5.1 13.3 0 18.4-2.5 2.5-5.9 3.8-9.2 3.8s-6.7-1.3-9.2-3.8l-75-79.9c-.8-.8-2.1-.8-2.9 0L313.7 297c-15.3 15.5-35.6 24.1-57.4 24.2-22.1.1-43.1-9.2-58.6-24.9l-17.6-17.9c-.8-.8-2.1-.8-2.9 0l-75 79.9c-2.5 2.5-5.9 3.8-9.2 3.8s-6.7-1.3-9.2-3.8c-5.1-5.1-5.1-13.3 0-18.4l75.3-80.2c.7-.8.7-2 0-2.8L51.4 147.3c-1.3-1.3-3.4-.4-3.4 1.4V368c0 17.6 14.4 32 32 32h352c17.6 0 32-14.4 32-32V148.7c0-1.8-2.2-2.6-3.4-1.4z"/><path d="M256 295.1c14.8 0 28.7-5.8 39.1-16.4L452 119c-5.5-4.4-12.3-7-19.8-7H79.9c-7.5 0-14.4 2.6-19.8 7L217 278.7c10.3 10.5 24.2 16.4 39 16.4z"/></svg>
									  </span>
                                </div>
                                <div class="form-group login__password">
                                    <label class="control-label sr-only required" for="customer_login_password">Password
                                    </label>
                                    <input id="customer_login_password" name="password" required="required" class="form-control input-lg" placeholder="Password" autocomplete="off" type="password">
                                    <span class="input-helper">
									<svg class="next-icon next-icon--color-slate-lighter next-icon--size-20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M376 192h-24v-46.7c0-52.7-42-96.5-94.7-97.3-53.4-.7-97.3 42.8-97.3 96v48h-24c-22 0-40 18-40 40v192c0 22 18 40 40 40h240c22 0 40-18 40-40V232c0-22-18-40-40-40zM270 316.8v68.8c0 7.5-5.8 14-13.3 14.4-8 .4-14.7-6-14.7-14v-69.2c-11.5-5.6-19.1-17.8-17.9-31.7 1.4-15.5 14.1-27.9 29.6-29 18.7-1.3 34.3 13.5 34.3 31.9 0 12.7-7.3 23.6-18 28.8zM324 192H188v-48c0-18.1 7.1-35.1 20-48s29.9-20 48-20 35.1 7.1 48 20 20 29.9 20 48v48z"/></svg>
								</span>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 auth-submit">
                                        <div class="login__remember-me">
                                            <input id="customer_login_rememberCustomerLogin" name="remember_token" checked="checked" value="true" type="checkbox">
                                            <label class="control-label" for="customer_login_rememberCustomerLogin"> Remember me
                                            </label>
                                        </div>
                                        <div class="login__forgot-password">
                                            <a href="/">
                                                Forgot your password?
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group login__submit">
                                    <button type="submit" id="customer_login_login" class="btn btn-primary btn-lg btn-block">Log in</button>
                                </div>
                            </form>
                        </div>
                        <div id="register" class="tab-pane fade">
                            <div class="login-register-content-block">
                                <form action="{{ action('UserController@store') }}" method="post" role="form" novalidate="novalidate" class="sign-up">
                                    @csrf
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label sr-only required" for="customer_registration_firstName">            Name
                                                </label>
                                                <input id="customer_registration_firstName" name="name" required="required" class="form-control input-lg" placeholder="Name" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label sr-only required" for="customer_registration_mobileNumber">            Mobile
                                        </label>
                                        <div class="row">
                                            <div class="col-3">
                                                <input id="customer_registration_mobileCountryCode" name="country_code" required="required" readonly="readonly" class="form-control input-lg" value="+88" type="text">
                                            </div>
                                            <div class="col-9">
                                                <input id="customer_registration_mobileNumber" name="phone_no" required="required" class="form-control input-lg" placeholder="Mobile" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label sr-only required" for="customer_registration_email">  Email
                                        </label>
                                        <input id="customer_registration_email" name="email" required="required" class="form-control input-lg" placeholder="Email" type="email">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label sr-only required" for="customer_registration_password_first">            Password
                                                </label>
                                                <input id="customer_registration_password_first" name="password" required="required" class="form-control input-lg" placeholder="Password" autocomplete="off" type="password">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label sr-only required" for="customer_registration_password_second">            Repeat password
                                                </label>
                                                <input id="customer_registration_password_second" name="password_confirmation" required="required" class="form-control input-lg" placeholder="Repeat password" autocomplete="off" type="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group cst-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label sr-only required" for="customer_registration_userType">User Type</label>
                                                <select id="customer_registration_userType" name="user_type" class="form-control input-lg" required>
                                                    <option value="user">User</option>
                                                    <option value="wholesaler">Wholesaler</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 auth-submit login_privacy">
                                            <input id="customer_registration_termsAndConditionsAccepted" name="accept_terms_condition" required="required" value="1" type="checkbox">
                                            <label class="control-label required" for="customer_registration_termsAndConditionsAccepted">            I have read and accepted the <a target="_blank" href="/contents/terms-and-conditions.htm">Terms and conditions</a> and <a target="_blank" href="/contents/privacy.htm">Privacy policy</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="customer_registration_registration" class="btn btn-primary btn-lg btn-block">Sign up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL-ENDS-HERE -->
<!-- ADS-MODAL-ENDS-HERE -->

{{--<div class="modal fade auth-modal no-guest-checkout" id="auth-modal" tabindex="-1" role="dialog">--}}
{{--    <div class="modal-dialog">--}}
{{--        <div class="modal-content">--}}
{{--            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
{{--            <div class="modal-body">--}}
{{--                <div class="content-block auth login js-login-register-flow">--}}
{{--                    <ul class="nav nav-pills nav-justified login-tabs no-guest-checkout">--}}
{{--                        <li class="login-tabs__sign-up"><a data-toggle="tab" href="#register">Sign up</a></li>--}}
{{--                        <li class="login-tabs__login active"><a data-toggle="tab" href="#login">Log in</a></li>--}}
{{--                    </ul>--}}
{{--                    <div class="login-register-content-block tab-content">--}}
{{--                        <div id="login" class="tab-pane active show">--}}
{{--                            <form action="{{ action('UserController@user_authenticate') }}" method="post" role="form" novalidate="novalidate" class="login">--}}
{{--                                @csrf--}}
{{--                                <div class="form-group login__email">--}}
{{--                                    <label class="control-label sr-only required" for="customer_login_email"> Email--}}
{{--                                    </label>--}}
{{--                                    <input id="customer_login_email" name="email" required="required" class="form-control input-lg" placeholder="Email" type="email">--}}
{{--                                    <span class="input-helper">--}}
{{--										<svg class="next-icon next-icon--color-slate-lighter next-icon--size-20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M460.6 147.3L353 256.9c-.8.8-.8 2 0 2.8l75.3 80.2c5.1 5.1 5.1 13.3 0 18.4-2.5 2.5-5.9 3.8-9.2 3.8s-6.7-1.3-9.2-3.8l-75-79.9c-.8-.8-2.1-.8-2.9 0L313.7 297c-15.3 15.5-35.6 24.1-57.4 24.2-22.1.1-43.1-9.2-58.6-24.9l-17.6-17.9c-.8-.8-2.1-.8-2.9 0l-75 79.9c-2.5 2.5-5.9 3.8-9.2 3.8s-6.7-1.3-9.2-3.8c-5.1-5.1-5.1-13.3 0-18.4l75.3-80.2c.7-.8.7-2 0-2.8L51.4 147.3c-1.3-1.3-3.4-.4-3.4 1.4V368c0 17.6 14.4 32 32 32h352c17.6 0 32-14.4 32-32V148.7c0-1.8-2.2-2.6-3.4-1.4z"/><path d="M256 295.1c14.8 0 28.7-5.8 39.1-16.4L452 119c-5.5-4.4-12.3-7-19.8-7H79.9c-7.5 0-14.4 2.6-19.8 7L217 278.7c10.3 10.5 24.2 16.4 39 16.4z"/></svg>--}}
{{--									  </span>--}}
{{--                                </div>--}}
{{--                                <div class="form-group login__password">--}}
{{--                                    <label class="control-label sr-only required" for="customer_login_password">Password--}}
{{--                                    </label>--}}
{{--                                    <input id="customer_login_password" name="password" required="required" class="form-control input-lg" placeholder="Password" autocomplete="off" type="password">--}}
{{--                                    <span class="input-helper">--}}
{{--									<svg class="next-icon next-icon--color-slate-lighter next-icon--size-20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M376 192h-24v-46.7c0-52.7-42-96.5-94.7-97.3-53.4-.7-97.3 42.8-97.3 96v48h-24c-22 0-40 18-40 40v192c0 22 18 40 40 40h240c22 0 40-18 40-40V232c0-22-18-40-40-40zM270 316.8v68.8c0 7.5-5.8 14-13.3 14.4-8 .4-14.7-6-14.7-14v-69.2c-11.5-5.6-19.1-17.8-17.9-31.7 1.4-15.5 14.1-27.9 29.6-29 18.7-1.3 34.3 13.5 34.3 31.9 0 12.7-7.3 23.6-18 28.8zM324 192H188v-48c0-18.1 7.1-35.1 20-48s29.9-20 48-20 35.1 7.1 48 20 20 29.9 20 48v48z"/></svg>--}}
{{--								</span>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <div class="col-sm-12 auth-submit">--}}
{{--                                        <div class="login__remember-me">--}}
{{--                                            <input id="customer_login_rememberCustomerLogin" name="remember_token" checked="checked" value="true" type="checkbox">--}}
{{--                                            <label class="control-label" for="customer_login_rememberCustomerLogin"> Remember me--}}
{{--                                            </label>--}}
{{--                                        </div>--}}
{{--                                        <div class="login__forgot-password">--}}
{{--                                            <a href="/">--}}
{{--                                                Forgot your password?--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group login__submit">--}}
{{--                                    <button type="submit" id="customer_login_login" name="customer_login[login]" class="btn btn-primary btn-lg btn-block">Log in</button>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                        <div id="register" class="tab-pane fade">--}}
{{--                            <div class="login-register-content-block">--}}
{{--                                <form action="/customer/register" method="post" role="form" novalidate="novalidate" class="sign-up">--}}
{{--                                    <input id="customer_registration__token" name="customer_registration[_token]" value="uAlzZhKZ_gDnWPntl4u3cl2wxySaQbKBgOKLQbSNw3k" type="hidden">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <label class="control-label sr-only required" for="customer_registration_firstName">            First name--}}
{{--                                                </label>--}}
{{--                                                <input id="customer_registration_firstName" name="customer_registration[firstName]" required="required" class="form-control input-lg" placeholder="First name" type="text">--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <label class="control-label sr-only required" for="customer_registration_lastName">            Last name--}}
{{--                                                </label>--}}
{{--                                                <input id="customer_registration_lastName" name="customer_registration[lastName]" required="required" class="form-control input-lg" placeholder="Last name" type="text">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label sr-only required" for="customer_registration_mobileNumber">            Mobile--}}
{{--                                        </label>--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-3">--}}
{{--                                                <input id="customer_registration_mobileCountryCode" name="customer_registration[mobileCountryCode]" required="required" readonly="readonly" class="form-control input-lg" value="91" type="text">--}}
{{--                                            </div>--}}
{{--                                            <div class="col-9">--}}
{{--                                                <input id="customer_registration_mobileNumber" name="customer_registration[mobileNumber]" required="required" class="form-control input-lg" placeholder="Mobile" type="text">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label sr-only required" for="customer_registration_email">  Email--}}
{{--                                        </label>--}}
{{--                                        <input id="customer_registration_email" name="customer_registration[email]" required="required" class="form-control input-lg" placeholder="Email" type="email">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group cst-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <label class="control-label sr-only required" for="customer_registration_password_first">            Password--}}
{{--                                                </label>--}}
{{--                                                <input id="customer_registration_password_first" name="customer_registration[password][first]" required="required" class="form-control input-lg" placeholder="Password" autocomplete="off" type="password">--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <label class="control-label sr-only required" for="customer_registration_password_second">            Repeat password--}}
{{--                                                </label>--}}
{{--                                                <input id="customer_registration_password_second" name="customer_registration[password][second]" required="required" class="form-control input-lg" placeholder="Repeat password" autocomplete="off" type="password">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <div class="col-sm-12 auth-submit login_privacy">--}}
{{--                                            <input id="customer_registration_termsAndConditionsAccepted" name="customer_registration[termsAndConditionsAccepted]" required="required" value="1" type="checkbox">--}}
{{--                                            <label class="control-label required" for="customer_registration_termsAndConditionsAccepted">            I have read and accepted the <a target="_blank" href="/contents/terms-and-conditions.htm">Terms and conditions</a> and <a target="_blank" href="/contents/privacy.htm">Privacy policy</a>--}}
{{--                                            </label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <button type="submit" id="customer_registration_registration" name="customer_registration[registration]" class="btn btn-primary btn-lg btn-block">Sign up</button>--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

{{-- Modal For Cart --}}
<div class="modal right cart-modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-arrow-right"></i></span></button>
                <h4 class="modal-title" id="myModalLabel2">Cart</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid p-0">
                    <div class="item-wrapper">
                        <checkout-component :items="items"></checkout-component>
                    </div>
                    <div class="col-12 p-0 total-box">
                        <h5 class="total-price text-uppercase d-flex">Total<span class="d-block price_num ml-auto">@{{ "US$" + total_price  }}</span></h5>
                        <a href="{{ action('ProductCartController@index') }}" class="btn btn-cart">View Cart</a>
                    </div>
                </div>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>


{{-- Modal For Session Message --}}
<div class="modal fade session no-guest-checkout" id="session-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success mb-0">
                <h4 class="modal-title">Success Message</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div id="success-message" class="tab-pane active show">
                    {{ session('success_message') }}
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal For Warning Message --}}
<div class="modal fade session no-guest-checkout" id="warning-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header alert alert-danger mb-0">
                <h4 class="modal-title">Warning Message</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div id="warning-message" class="tab-pane active show">
                    {{ session('warning_message') }}
                </div>
            </div>

        </div>
    </div>
</div>

{{-- Modal Fro Added Chart--}}
<!-- The Modal -->
<div class="modal fade" id="card_added">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ADDED TO CART</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <img id="cart_product_image" src="{{ '/storage/' .session('cart_product_image') }}" style="width: 100px" class="img-fluid">
                    </div>
                    <div class="col-md-8">
                        <p><span id="cart_product_name"></span> was successfully added to your shopping cart.</p>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <a href="{{ action('ProductCartController@index') }}" class="btn btn-primary">VIEW CHART & CHECKOUT</a>
            </div>

        </div>
    </div>
</div>
