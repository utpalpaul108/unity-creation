<div class="modal right cart-modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-arrow-right"></i></span></button>
                <h4 class="modal-title" id="myModalLabel2">Cart</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid p-0">
                    <div class="item-wrapper">
                        <ul class="list-inline items_list">
                            <li>
                                <figure class="product-item" style="background:url('/images/item-img.png')"></figure>
                                <div class="item-info">
                                    <h5 class="item-name">YOUR BRANDS PRODUCT<span class="d-block item-quantity">QTY 6</span></h5>
                                    <p class="item-price"><span class="old-price">$1.00</span><span class="new-price">$0.50</span></p>
                                </div>
                                <a href="#" class="delete-link">×</a>
                            </li><!-- ITEM-BOX-ENDS -->
                            <li>
                                <figure class="product-item" style="background:url('/images/item-img.png')"></figure>
                                <div class="item-info">
                                    <h5 class="item-name">YOUR BRANDS PRODUCT<span class="d-block item-quantity">QTY 6</span></h5>
                                    <p class="item-price"><span class="old-price">$1.00</span><span class="new-price">$0.50</span></p>
                                </div>
                                <a href="#" class="delete-link">×</a>
                            </li><!-- ITEM-BOX-ENDS -->

                        </ul>
                    </div>
                    <div class="col-12 p-0 total-box">
                        <h5 class="total-price text-uppercase d-flex">Total<span class="d-block price_num ml-auto">US$ 137.94</span></h5>
                        <a href="view-cart.php" class="btn btn-cart">View Cart</a>
                    </div>
                </div>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
