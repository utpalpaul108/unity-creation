<h3 class="text-uppercase">({{ $comments->count() }}) Comments</h3>
<div class="blog-message">
    <ul class="list-unstyled">
        @foreach($comments as $comment)
            <li class="media mt-4 mb-3 mr-4">
                <img src="@if(isset($replay->created_by['profile_image']) && !is_null($replay->created_by['profile_image'])){{ '/storage/' .$replay->created_by['profile_image'] }} @else {{ '/images/autthor-image.jpg' }} @endif" class="mr-3 img-fluid" alt="...">
                <div class="media-body">
                    <!--breadcrumb-->
                    <nav aria-label="breadcrumb blog-details-brdcm ">
                        <ol class="breadcrumb blog-details-ol mb-0">
                            <li class="breadcrumb-item"><a href="#">{{ $comment->commented_by['name'] }}</a></li>
                            <li class="breadcrumb-item"><a href="#">{{ $comment->created_at->diffForHumans() }}</a></li>
                        </ol>
                    </nav>
                    {!! $comment->comment !!}
                    <div class="replay-message mt-3">
                        <button type="button" class="replay-blog-msg">
                            <i class="fa fa-reply"></i><span class="replay-text-blog-details">Replay</span>
                        </button>
                    </div>
                    <div class="replay-message-box-post my-2">
                        <form method="post">
                            <div class="form-group">
                                <textarea rows="3" class="form-control replay-comment-bottom comment_replay" type="text" name="user-message" placeholder="Enter Your Replay"></textarea>
                            </div>
                            <div class="form-group">
                                <a href="javascript:void(0)" class="btn submit-btn text-uppercase ic_replay_message" data-comment-id="{{ $comment->id }}">Submit Now</a>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            @if(!is_null($comment->replies))
                @foreach($comment->replies as $replay)
                    <li class="media mb-4 ml-4 replay-msg-2position">
                        <img src="@if(isset($replay->created_by['profile_image']) && !is_null($replay->created_by['profile_image'])){{ '/storage/' .$replay->created_by['profile_image'] }} @else {{ '/images/autthor-image.jpg' }} @endif" class="mr-3 img-fluid" alt="...">
                        <div class="media-body">
                            <!--breadcrumb-->
                            <nav aria-label="breadcrumb blog-details-brdcm ">
                                <ol class="breadcrumb blog-details-ol mb-0">
                                    <li class="breadcrumb-item"><a href="#">{{ $replay->created_by['name'] }}</a></li>
                                    <li class="breadcrumb-item"><a href="#"> {{ $replay->created_at->diffForHumans() }}</a></li>
                                </ol>
                            </nav>
                            {!! $replay->replay !!}
                            <div class="replay-message mt-3">
                                <a href="javascript:void(0)" class="replay-blog-msg"><i class="fa fa-reply"></i><span class="replay-text-blog-details">Replay</span></a>
                            </div>
                            <div class="replay-message-box-post my-2">
                                <form method="post">
                                    <div class="form-group">
                                        <textarea rows="3" class="form-control replay-comment-bottom comment_replay" type="text" name="user-message" placeholder="Enter Your Comments"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <a href="javascript:void(0)" class="btn submit-btn text-uppercase ic_replay_message" data-comment-id="{{ $comment->id }}">Submit Now</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </li>
                @endforeach
            @endif
        @endforeach
    </ul>
</div>
