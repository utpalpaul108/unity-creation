<section class="trending-product pt-3 pb-3">
    <div class="container">
        <h3 class="text-uppercase section-head mb-5 text-center">
            Trending products
        </h3>
        <div class="slide-bx col-12 p-0" id="demos">
            <div class="col-sm-12 pl-5 pr-5">
                <div class="owl-carousel owl-theme">
                    @foreach($products as $product)
                        <div class="item single-gallery">
                            <div class="thumb">
                                <a href="{{ action('ProductController@product_details',$product->id) }}" class="item-link">
                                    <img src="{{ '/storage/' .$product->default_image }}" alt="">
                                    <div class="item-info align-items-center justify-content-center text-center">
                                        <h4 class="item-name"> {{ $product->title }}</h4>
{{--                                        <p class="item-category mb-0">{{ $product->category['name'] }}</p>--}}
                                        <h5 class="item-price">$ {{ $product->average_price }}</h5>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
