@inject('request', 'Illuminate\Http\Request')

<ul class="navbar-nav u-header__navbar-nav sub-nav mr-auto">
    @foreach($items as $index=>$item)
        <li class="nav-item {{ request()->is($item->url) ? 'active' : '' }} {{ !$item->children->isEmpty() ? 'dropdown' : '' }}" >
            @if(!$item->children->isEmpty())
                <a class="nav-item nav-link mr-md-2" href="{{ $item->url }}" id="bd-versions{{ $index }}" aria-haspopup="true" aria-expanded="false">
                    {{ $item->title }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                @include('partials.menu-items', ['items' => $item->children, 'index'=>$index, 'menu_image'=>$item->menu_image])
            @else
                <a href="{{ $item->url }}">{{ $item->title }}</a>
            @endif
        </li>
    @endforeach
</ul>
