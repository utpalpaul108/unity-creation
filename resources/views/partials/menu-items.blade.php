
<div class="dropdown-menu category-3 mega-menu {{ $submenu ?? '' }}" aria-labelledby="bd-versions" style="background:url({{'/storage/' .$menu_image}})">
    <div class="col-6 menuContainer {{ isset($submenu) ? '' : 'border-left' }}">
        <ul class="list-group list-group-flush list-group-no-border list-group-sm">
            @foreach($items as $item)
                <li>
                    @if(!$item->children->isEmpty())
                        <a class="list-group-item list-group-item-action menu-btn" href="{{ $item->url }}">{{ $item->title }}<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        @include('partials.menu-items', ['items' => $item->children, 'index'=>$index, 'submenu'=>'sub-menu', 'menu_image'=>$item->menu_image])
                    @else
                        <a href="{{ $item->url }}" class="list-group-item list-group-item-action"><strong>{{ $item->title }}</strong></a>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>
