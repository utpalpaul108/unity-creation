@extends('layout.app')

@section('page_title',' | Product Details')

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">View Cart</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="cart-page p-0">
            <div class="container">
                <div class="col-12 p-0 text-center">
                    <h3 class="text-uppercase section-head mb-5 text-center">View Cart</h3>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span class="pull-left"><strong><i class="fa fa-shopping-cart"></i></strong> {{ count($product_charts) }} Items in add to cart</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
                <div class="col-12 cart-items p-0">
                    <table id="cart" class="table table-hover table-condensed">
                        <tbody>
                            @php
                                $total_amount=0;
                            @endphp
                            @foreach($product_charts as $index=>$product_chart)
                                @php
                                    $product_details=product_details($product_chart['id']);
                                    $total_amount+=$product_details->price*$product_chart['quantity'];
                                @endphp
                                <tr>
                                    <td data-th="Product">
                                        <div class="row">
                                            <div class="col-sm-2 hidden-xs"><img src="{{ '/storage/' .$product_details->product_image }}" alt="product image" class="img-fluid item-pro"></div>
                                            <div class="col-sm-10">
                                                <h5 class="m-0 text-uppercase">{{ $product_details->title ?? '' }}</h5>
                                                <p class="item-info m-0">Style #: {{ $product_details->product_code }} / Size {{ $product_chart['size'] ?? '' }}</p>
                                                <div class="star-list">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star empty"></span>
                                                </div>
                                                <p>(Based on 55 Reviews)</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Quantity">
                                        <h5 class="text-uppercase">Quantity:</h5>
                                        <input type="number" class="form-control text-center" min="1" value="{{ $product_chart['quantity'] ?? '' }}">
                                    </td>
                                    <td data-th="Price"><h5 class="text-uppercase">Unit price:</h5>${{ $product_details->price ?? '' }}</td>
                                    <td data-th="Subtotal" class="text-center"><h5 class="text-uppercase">Total price:</h5>${{ $product_details->price*$product_chart['quantity'] }}</td>
                                    <td class="actions" data-th="">
                                        <a href="{{ action('ProductCartController@destroy',$index) }}" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-md-12 p-0">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="head-left">Enter vouchure  code if you have one. <a href="#">Apply Here</a></h5>
                                <form class="d-flex coupon-form" action="{{ action('CouponController@usage') }}" method="post">
                                    @csrf
                                    <input type="text" class="form-control" name="coupon_code" placeholder="ENTER COUPON CODE" required>
                                    <button class="btn-sub" type="submit">Apply Now</button>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="card total-card">
                                    <div class="totals card-body">
                                        <h3 class="text-uppercase heading-cart">BEST Feel</h3>
{{--                                        <div class="totals-item row">--}}
{{--                                            <label>Est. Delivery</label>--}}
{{--                                            <div class="totals-value" id="cart-subtotal">22 - 25 April, 2019</div>--}}
{{--                                        </div>--}}
                                        <div class="totals-item row">
                                            <label>Subtotal</label>
                                            <div class="totals-value" id="cart-tax">${{ $total_amount }}</div>
                                        </div>
                                        <div class="totals-item row">
                                            <label>Shipping Charge</label>
                                            <div class="totals-value" id="cart-shipping">$0</div>
                                        </div>
                                        <div class="totals-item totals-item-total row">
                                            <label>Tax</label>
                                            <div class="totals-value" id="cart-total">$0</div>
                                        </div>
                                        <div class="totals-item totals-item-total row">
                                            <label>Discount</label>
                                            <div class="totals-value" id="cart-total">${{ $discount_amount }}</div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="totals-item totals-item-total row">
                                            <label>Total</label>
                                            <div class="totals-value" id="cart-total">${{ $total_amount - $discount_amount }}</div>
                                        </div>
                                        <div class="col-md-12 text-center btn-groups p-0">
                                            @if(count($product_charts)>0)
                                                <a href="{{ action('ProductCartController@product_checkout') }}" class="btn btn-lg text-uppercase">process to Checkout Now</a>
                                            @endif
                                            <a href="{{ action('ProductController@index') }}" class="btn btn-lg text-uppercase">continue shopping</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
