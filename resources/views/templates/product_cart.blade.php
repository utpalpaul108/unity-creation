@extends('layout.app')

@section('page_title',' | Product Details')

@section('contents')
    <main class="page-main" >
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">View Cart</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="cart-page p-0">
            <div class="container">
                <div class="col-12 p-0 text-center">
                    <h3 class="text-uppercase section-head mb-5 text-center">View Cart</h3>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span class="pull-left"><strong><i class="fa fa-shopping-cart"></i></strong> {{ count($product_charts) }} Items in add to cart</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
                <div class="col-12 cart-items p-0">
                    <cart-component :items="items" @update-cart="updateCart"></cart-component>
                    <div class="col-md-12 p-0">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="head-left">Enter vouchure  code if you have one. <a href="#">Apply Here</a></h5>
                                <form class="d-flex coupon-form" action="{{ action('CouponController@usage') }}" method="post">
                                    @csrf
                                    <input type="text" class="form-control" name="coupon_code" placeholder="ENTER COUPON CODE" required>
                                    <button class="btn-sub" type="submit">Apply Now</button>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="card total-card">
                                    <div class="totals card-body">
                                        <h3 class="text-uppercase heading-cart">BEST Feel</h3>
{{--                                        <div class="totals-item row">--}}
{{--                                            <label>Est. Delivery</label>--}}
{{--                                            <div class="totals-value" id="cart-subtotal">22 - 25 April, 2019</div>--}}
{{--                                        </div>--}}
                                        <div class="totals-item row">
                                            <label>Subtotal</label>
                                            <div class="totals-value" id="cart-tax">@{{ "$" + subtotal_price  }}</div>
                                        </div>
                                        <div class="totals-item row">
                                            <label>Shipping Charge</label>
                                            <div class="totals-value" id="cart-shipping">$0</div>
                                        </div>
                                        <div class="totals-item totals-item-total row">
                                            <label>Tax</label>
                                            <div class="totals-value" id="cart-total">$0</div>
                                        </div>
                                        <div class="totals-item totals-item-total row">
                                            <label>Discount</label>
                                            <div class="totals-value" id="cart-total">@{{ "$" + discount  }}</div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="totals-item totals-item-total row">
                                            <label>Total</label>
                                            <div class="totals-value" id="cart-total">@{{ "$" + total_price  }}</div>
                                        </div>
                                        <div class="col-md-12 text-center btn-groups p-0">
                                            @if(count($product_charts)>0)
                                                <a href="{{ action('ProductCartController@product_checkout') }}" class="btn btn-lg text-uppercase">process to Checkout Now</a>
                                            @endif
                                            <a href="{{ action('ProductController@index') }}" class="btn btn-lg text-uppercase">continue shopping</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
