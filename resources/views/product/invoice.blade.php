<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Order Invoice</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
            font-family: Arial;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 60px 20px 0;
            background: url('./images/dimension.png');
        }

        #project {
            float: left;
        }

        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {
            float: right;
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            margin-right: 60px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: center;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.2em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 8px 0;
            text-align: center;
            margin-right: 60px;
        }

        .text-right{
            text-align: right;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img src="./images/logo.png">
    </div>
    <h1>ORDER INVOICE</h1>
    <div id="project">
        <div><span>USER : </span> {{ Auth::user()->name }}</div>
        <div><span>ORDER ID</span> {{ $order_info['order_id'] }}</div>
        <div><span>EMAIL</span> <a href="mailto:john@example.com">{{ Auth::user()->email }}</a></div>
        <div><span>ORDER DATE</span> {{ \Carbon\Carbon::now()->format('Y-m-d') }}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th class="service">PRODUCT CODE</th>
            <th class="desc">PRODUCT TITLE</th>
            <th>PRICE</th>
            <th>QTY</th>
            <th>DISCOUNT</th>
            <th>TOTAL</th>
        </tr>
        </thead>
        <tbody>
        @foreach($order_items as $order_item)
            <tr>
                <td class="service">{{ $order_item['product_code'] }}</td>
                <td class="desc">{{ $order_item['product_title'] }}</td>
                <td class="unit">${{ $order_item['price'] }}</td>
                <td class="qty">{{ $order_item['quantity'] }}</td>
                <td class="qty">${{ $order_item['discount'] }}</td>
                <td class="total">${{ $order_item['total_price'] }}</td>
            </tr>
        @endforeach

        <tr>
            <td colspan="5" class="grand total text-right" >GRAND TOTAL</td>
            <td class="grand total">${{ $order_total_price }}</td>
        </tr>
        </tbody>
    </table>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">You can contact with us for any query.</div>
    </div>
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
