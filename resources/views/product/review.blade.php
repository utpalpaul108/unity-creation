@extends('layout.app')

@section('page_title',' | Product Review')

@section('style')
    <style>
        {{--        For Review Rating --}}
        *{
            margin: 0;
            padding: 0;
        }
        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }
        .rate:not(:checked) > input {
            position:absolute;
            left:-9999px;
        }
        .rate:not(:checked) > label {
            float:right;
            width:1em;
            /*overflow:hidden;*/
            white-space:nowrap;
            cursor:pointer;
            font-size:30px;
            color:#ccc;
            position: relative;
            left: -47px;
        }
        .rate:not(:checked) > label:before {
            content: '★ ';
        }
        .rate > input:checked ~ label {
            color: #ffc700;
        }
        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;
        }
        .rate > input:checked + label:hover,
        .rate > input:checked + label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }
        .table-hover tbody tr:hover {
            background-color: #f8fafc;
        }

    </style>
@endsection

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Product Review</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="profiles-payments py-0">
            <div class="container">
                <div class="profile-image">
                    <img src="@if(!is_null(Auth::user()->banner_image)){{ '/storage/' .Auth::user()->banner_image }} @else{{ '/images/profile-payments-profile-pic.jpg' }} @endif" class="img-fluid banner-profile-img" alt="">
                    <a href="" type="btn" class="small-image-profile-btn"><img src="@if(!is_null(Auth::user()->profile_image)){{ '/storage/' .Auth::user()->profile_image }} @else{{ '/images/profile-small-pic-payments.png' }} @endif" class="img-fluid" alt="" style="    margin-top: -70px;"></a>
                    <a href="{{ action('UserController@edit_profile') }}" type="btn" class="banner-image-profile-btn text-uppercase">Edit profile</a>
                </div>
                <div class="form-area-profile-settings">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="profile-payments-content-left">
                                <h4 class="font-weight-bold">{{ Auth::user()->name }}</h4>
                                <p class="user-destination">{{ Auth::user()->profession }}</p>
                                <ul class="footer-links list-inline no-mb profile-payments-ul">
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        {{ Auth::user()->address }}
                                    </li>
                                    <li class="clear"><i class="fa fa-phone"></i> {{ Auth::user()->country_code .' ' .Auth::user()->phone_no }}</li>
                                    <li><i class="fa fa-envelope"></i>  {{ Auth::user()->email }}</li>
                                </ul>
                                <p class="font-weight-bold site-main-color mt-3">Member since {{ Auth::user()->created_at->format('D M y') }}</p>
                            </div>
                        </div>

                        <div class="col-md-9 col-lg-9">
                            <!-- Tabs start -->
                            <div class="tabs-profile-payments">
                                <div class="tab-content" id="pills-tabContent">
                                    <!-- Tabs 1 content start -->
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <div id="purchase-history">
                                            <div class="col-12 cart-items p-0">
                                                <table id="cart" class="table table-hover table-condensed purchase-history-table">
                                                    <tbody>
                                                        <tr>

                                                            <td data-th="Product" style="padding-left:30px;">
                                                                <div class="row">
                                                                    <div class="col-sm-2 hidden-xs flex-mid-inner"><div class="col-inner-single">
                                                                            <img src="{{ '/storage/' .$product->default_image }}" alt="Product Image" class="img-fluid item-pro">
                                                                        </div></div>
                                                                    <div class="col-sm-4 flex-mid-inner">
                                                                        <div class="col-inner-single">
                                                                            <div class="purchase-product-name-inner">
                                                                                <h5 class="m-0 text-uppercase payment-history-h5">{{ $product->title }}</h5>
                                                                                <p class="item-info m-0">Style #: {{ $product->product_code }}</p>
                                                                                <div class="star-list">
                                                                                    <span class="fa fa-star"></span>
                                                                                    <span class="fa fa-star"></span>
                                                                                    <span class="fa fa-star"></span>
                                                                                    <span class="fa fa-star"></span>
                                                                                    <span class="fa fa-star empty"></span>
                                                                                </div>
                                                                                <p class="mb-0">(Based on 55 Reviews)</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 flex-mid-inner">
                                                                        <form action="{{ action('ProductReviewController@store') }}" method="post">
                                                                            @csrf
                                                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                                            <div class="rate">
                                                                                <input type="radio" id="star5" name="rating" value="5" @if(isset($product_review->rating) && ($product_review->rating ==5)){{ 'checked' }} @endif />
                                                                                <label for="star5" title="text"></label>
                                                                                <input type="radio" id="star4" name="rating" value="4" @if(isset($product_review->rating) && ($product_review->rating ==4)){{ 'checked' }} @endif />
                                                                                <label for="star4" title="text"></label>
                                                                                <input type="radio" id="star3" name="rating" value="3" @if(isset($product_review->rating) && ($product_review->rating ==3)){{ 'checked' }} @endif />
                                                                                <label for="star3" title="text"></label>
                                                                                <input type="radio" id="star2" name="rating" value="2" @if(isset($product_review->rating) && ($product_review->rating ==2)){{ 'checked' }} @endif />
                                                                                <label for="star2" title="text"></label>
                                                                                <input type="radio" id="star1" name="rating" value="1" @if(isset($product_review->rating) && ($product_review->rating ==1)){{ 'checked' }} @endif />
                                                                                <label for="star1" title="text"></label>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <textarea class="form-control" rows="4" name="review">{{ $product_review->review ?? '' }}</textarea>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <button type="submit" class="btn btn-sm btn-primary float-right">Send</button>
                                                                            </div>
                                                                        </form>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tabs end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
