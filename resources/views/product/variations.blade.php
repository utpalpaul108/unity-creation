<div class="slider-wrapper">
    <div class="slider-for">
        @php($product_images=explode(',',$product_images))
        @foreach($product_images as $product_image)
            @if(!empty($product_image))
                <div class="slider-for__item ex1" data-src="{{ $product_image }}">
                    <img src="{{ str_replace('thumbs/','',$product_image ) }}" alt="" />
                </div>
            @endif
        @endforeach
    </div>

    <div class="slider-nav">
        @foreach($product_images as $product_image)
            @if(!empty($product_image))
                <div class="slider-nav__item">
                    <img src="{{ $product_image }}" alt="" />
                </div>
            @endif
        @endforeach
    </div>
    <input type="hidden" name="productVariationId" value="{{ $variation_id }}" v-model="productVariationId">
</div>
