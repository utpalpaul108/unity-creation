@foreach($product_sizes as $index=>$size)
    <li>
        <input name="size" class="radio" type="radio" value="{{ $size }}" {{ ($index == 0) ? 'checked' : '' }}>
        <label>{{ $size }}</label>
    </li>
@endforeach
