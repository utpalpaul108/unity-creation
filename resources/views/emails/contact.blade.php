@component('mail::message')
# From Unity Creation Contact Message

<br>
<b>Title : </b>{{ $user_info['title'] }}<br>
<b>Name : </b>{{ $user_info['name'] }}<br>
<b>Message : </b>{{ $user_info['message'] }}<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
