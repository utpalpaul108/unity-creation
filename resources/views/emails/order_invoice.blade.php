@component('mail::message')
    # Unity Creation order invoice

    Thanks, for joining with us.
    Your order ID : 123456
    Please download your invoice.

    Regards,
    {{ config('app.name') }}
@endcomponent
