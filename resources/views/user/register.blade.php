@extends('layout.app')

@section('page_title',' | Edit Profile')

@section('contents')
    <main class="page-main-register">
        <section class="loginRegister">
            <div class="col-md-6 col-lg-6 col-sm-6 offset-md-3 offset-lg-3">
                <div class="loginRegister-Innersection">
                    <!-- Tabs start -->
                    <div class="tabs-profile-payments tabs-register-login">
                        <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">  Register</a>
                            </li>
                        </ul>

                        <!-- Tab 1 Login content -->
                        <div class="tab-content" id="pills-tabContent">
{{--                            @include('flash::message')--}}
{{--                            @if ($errors->any())--}}
{{--                                <div class="alert alert-danger">--}}
{{--                                    <ul>--}}
{{--                                        @foreach ($errors->all() as $error)--}}
{{--                                            <li>{{ $error }}</li>--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            @endif--}}

                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="content-register-login text-center">
                                    <h3 class="text-uppercase regLogH3 font-weight-bold text-center">Login</h3>
                                    <p class="text-center stay-with-p">Stay with us to know about fashion and Take your
                                        best one form our store</p>
                                    <div class="form-area-profile-settings">
                                        <form action="{{ action('UserController@user_authenticate') }}" class="profiles-setting-form profile-payments-form" style="margin-top:38px" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control RegLogInput" id="emailLogReg" placeholder="Email Address" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control RegLogInput" id="PhoneLogReg" placeholder="Password" required>
                                            </div>
                                            <div class="form-group" style="margin-bottom:7px;">
                                                <div class="col-md-12" style='padding-left:0;padding-right:0;'>
                                                    <button style="width:100%;padding-top:14px;padding-bottom:14px;font-weight:normal" type="submit" class="text-uppercase btn btn-primary profile-settings-btn-form payment-profile-form-btn mb-2">Login Now</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Tabs 1 login content end -->
                            <!-- Tabs 2 content start -->
                            <div class="tab-pane fade " id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="content-register-login text-center">
                                    <h3 class="text-uppercase regLogH3 font-weight-bold text-center">register</h3>
                                    <p class="text-center stay-with-p">Stay with us to know about fashion and Take your
                                        best one form our store</p>
                                    <div class="form-area-profile-settings">
                                        <form action="{{ action('UserController@store') }}" method="post" class="profiles-setting-form profile-payments-form" style="margin-top:38px">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" name="name" class="form-control RegLogInput" id="NamelLogReg" placeholder="Name" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control RegLogInput" id="emailLogReg" placeholder="Email Address" required>
                                            </div>
                                            <div class="form-group">
                                                <div class="row" style="margin: 0px .5px">
                                                    <input type="text" class="form-control RegLogInput col-md-3" value="+88" name="country_code" readonly="readonly" placeholder="Country Code" required>
                                                    <input type="number" class="form-control RegLogInput col-md-9" name="phone_no" id="PhoneLogReg" placeholder="Phone Number" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control RegLogInput" id="PhoneLogReg" placeholder="Password" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password_confirmation" class="form-control RegLogInput" id="PhoneLogReg" placeholder="Retype Password" required>
                                            </div>
                                            <div class="form-group">
                                                <input id="customer_registration_termsAndConditionsAccepted" name="accept_terms_condition" required="required" value="1" type="checkbox">
                                                <label class="control-label required" for="customer_registration_termsAndConditionsAccepted">I have read and accepted the <a target="_blank" href="/contents/terms-and-conditions.htm">Terms and conditions</a> and <a target="_blank" href="/contents/privacy.htm">Privacy policy</a></label>
                                            </div>
                                            <div class="form-group" style="margin-bottom:7px;">
                                                <div class="col-md-12" style='padding-left:0;padding-right:0;'>
                                                    <button style="width:100%;padding-top:14px;padding-bottom:14px;font-weight:normal" type="submit" class="text-uppercase btn btn-primary profile-settings-btn-form payment-profile-form-btn mb-2">Register Now</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Tabs 2 content end -->
                            <!-- No need tab 3 content division -->
                        </div>
                        <!-- Tabs end -->
                    </div>
                </div>
        </section>
    </main>
@endsection

@section('script')
    <script>
        $('div.alert').delay(3000).fadeOut(350);
    </script>
@endsection
