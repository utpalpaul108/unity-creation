@extends('layout.app')

@section('page_title',' | Profile')

@section('contents')
    <main class="page-main">
        <section class="bread-links p-0">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User Profiles</li>
                    </ol>
                </nav>
            </div>
        </section>
        <section class="profiles-payments py-0">
            <div class="container">
                <div class="profile-image">
                    <img src="@if(!is_null($user->banner_image)){{ '/storage/' .$user->banner_image }} @else{{ '/images/Profiles_Setting.jpg' }} @endif" class="img-fluid banner-profile-img" alt="">
                    <a href="" type="btn" class="small-image-profile-btn"><img src="@if(!is_null($user->profile_image)){{ '/storage/' .$user->profile_image }} @else{{ '/images/profile-setting-small-img.png' }} @endif" class="img-fluid" alt="" style="    margin-top: -70px;"></a>
                    <a href="{{ action('UserController@edit_profile') }}" type="btn" class="banner-image-profile-btn text-uppercase">Edit profile</a>
                </div>
                <div class="form-area-profile-settings">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="profile-payments-content-left">
                                <h4 class="font-weight-bold">{{ $user->name }}</h4>
                                <p class="user-destination">{{ $user->profession }}</p>
                                <ul class="footer-links list-inline no-mb profile-payments-ul">
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        {{ $user->address }}
                                    </li>
                                    <li class="clear"><i class="fa fa-phone"></i> {{ $user->country_code .' ' .$user->phone_no }}</li>
                                    <li><i class="fa fa-envelope"></i>  {{ $user->email }}</li>
                                </ul>
                                <p class="font-weight-bold site-main-color mt-3">Member since {{ $user->created_at->format('d M Y') }}</p>
                            </div>
                        </div>

                        <div class="col-md-9 col-lg-9">
                            <!-- Tabs start -->
                            <div class="tabs-profile-payments">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Purchases History</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ action('UserController@edit_profile') }}" class="nav-link" id="pills-contact-tab"  aria-controls="pills-contact" aria-selected="false"> Profile Setting</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <!-- Tabs 1 content start -->
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <div id="purchase-history">
                                            <div class="col-12 cart-items p-0">
                                                <table id="cart" class="table table-hover table-condensed purchase-history-table">
                                                    <tbody>
                                                    @php($sl=1)
                                                    @foreach($product_orders as $product_order)
                                                        @foreach($product_order->order_details as $order)
                                                            <tr>
                                                                <td data-th="Purchase-Item" id="purchase-increment-td"><h3>{{ $sl }}</h3></td>

                                                                <td data-th="Product" style="padding-left:30px;">
                                                                    <div class="row">
                                                                        <div class="col-sm-2 hidden-xs flex-mid-inner"><div class="col-inner-single">
                                                                                <img src="{{ '/storage/' .$order->product['default_image'] }}" alt="Product Image" class="img-fluid item-pro">
                                                                            </div></div>
                                                                        <div class="col-sm-4 flex-mid-inner">
                                                                            <div class="col-inner-single">
                                                                                <div class="purchase-product-name-inner">
                                                                                    <h5 class="m-0 text-uppercase payment-history-h5"><a href="{{ action('ProductController@product_details',$order->product['id']) }}">{{ $order->product['title'] }}</a></h5>
                                                                                    <p class="item-info m-0">Style #: {{ $order->product['product_code'] }} / Size {{ $order->product_size }}</p>
                                                                                    <div class="star-list">
                                                                                        <span class="fa fa-star {{ ($order->product->review['avg_rating'] <1) ? 'empty' : '' }}"></span>
                                                                                        <span class="fa fa-star {{ ($order->product->review['avg_rating'] <2) ? 'empty' : '' }}"></span>
                                                                                        <span class="fa fa-star {{ ($order->product->review['avg_rating'] <3) ? 'empty' : '' }}"></span>
                                                                                        <span class="fa fa-star {{ ($order->product->review['avg_rating'] <4) ? 'empty' : '' }}"></span>
                                                                                        <span class="fa fa-star {{ ($order->product->review['avg_rating'] <5) ? 'empty' : '' }}"></span>
                                                                                    </div>
                                                                                    <p class="mb-0">(Based on {{ $order->product->review['total_review'] }} Reviews)</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2 flex-mid-inner">
                                                                            <div class="col-inner-single">
                                                                                <h6 class="text-uppercase">Price
                                                                                </h6>${{ $order->total_price }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2 flex-mid-inner">
                                                                            <div class="col-inner-single">
                                                                                <h6 class="text-uppercase">Bought
                                                                                </h6>{{ $order->product_quantity }} Units
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2 flex-mid-inner">
                                                                            <div class="col-inner-single">
                                                                                <a href="{{ action('ProductReviewController@create',$order->product['id']) }}" class="btn btn-success btn-sm">Provide Review</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @php($sl++)
                                                        @endforeach
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        {{--                                        <div class="col-md-12 text-center justify-align-center bottom-pages pagination-blog">--}}
                                        {{--                                            <div class="row">--}}
                                        {{--                                                <ul class="pagination mx-auto">--}}
                                        {{--                                                    <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                                        {{--                                                    <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                        {{--                                                    <li class="page-item active"><a class="page-link" href="#">3</a></li>--}}
                                        {{--                                                    <li class="page-item"><a class="page-link" href="#">4</a></li>--}}
                                        {{--                                                    <li class="page-item"><a class="page-link" href="#">5</a></li>--}}
                                        {{--                                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>--}}
                                        {{--                                                </ul>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </div>
                                <!-- Tabs end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
