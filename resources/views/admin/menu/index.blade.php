@extends('admin.layout.app')

@section('page_title','Admin | Menu')
@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection
@section('contents')
    <!-- BEGIN .sa-page-breadcrumb -->
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        {{--        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>--}}
        {{--        <li class="breadcrumb-item"><a href="{{ action('Admin\MenuController@index') }}">Menu Builder</a></li>--}}
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-desktop"></i> Dashboard <span>> Menu Builder</span></h1>
            </div>
        </div>
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget well" id="wid-id-0">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <div class="widget-header">
                                <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                <h2>My Data </h2>
                            </div>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">

                                <div id="nestable-menu">
                                    <button type="button" class="btn btn-default" data-action="expand-all">
                                        Expand All
                                    </button>
                                    <button type="button" class="btn btn-default" data-action="collapse-all">
                                        Collapse All
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <h6>Manage Menu <span class="add-menu-items"><i class="fa fa-plus-circle"></i></span></h6>

                                        <div class="dd" id="nestable">
                                            <ol class="dd-list">
                                                @foreach($items as $item)
                                                    <li class="dd-item" data-id="{{ $item->id }}">
                                                        <div class="pull-right" style="padding: 8px 0px;">
                                                            <div class="btn btn-xs btn-danger pull-right button-delete" style="margin-left: 5px ; margin-right: 5px" data-id="{{ $item->id }}">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </div>
                                                            <div class="btn btn-xs btn-primary pull-right button-edit" style="margin-right: 5px" data-id="{{ $item->id }}" data-title="{{ $item->title }}" data-target="{{ $item->target }}" data-url="{{ $item->url }}" data-image="{{ $item->menu_image }}">
                                                                <i class="fa fa-edit"></i> Edit
                                                            </div>
                                                        </div>
                                                        <div class="dd-handle">
                                                            {{ $item->title }}
                                                        </div>
                                                        @if(!$item->children->isEmpty())
                                                            @include('admin.menu.menu-items', ['items' => $item->children])
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">

                                        <div class="widget-body">
                                            <div id="accordion" class="add-menu">
                                                <div class="card">
                                                    <legend>Add Menu Item</legend>
                                                    <a class="card-header" href="javascript:void(0)" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
                                                        Pages
                                                        <i class="fa fa-lg fa-angle-up pull-right"></i>
                                                    </a>

                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                        <div class="card-body">
                                                            <form class="form-group">
                                                                <input class="form-control mt-2 title" name="title" placeholder="Menu Title" type="text" id="title" required>
                                                                <select class="form-control mt-1 url" name="pages">
                                                                    @foreach($pages as $page)
                                                                        <option value="/{{ $page->slug }}">{{ $page->page_title }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select class="form-control new-menu-item mt-1 target" name="target" required>
                                                                    <option value="_self">Current Tab</option>
                                                                    <option value="_blank">New Tab</option>
                                                                </select>
                                                                <input type="file" name="menu_image" class="mt-2 menu_image">
                                                                <button class="btn btn-primary mt-2 mb-2 pull-right addItem" type="button" onclick="addItems(this)">Add To Menu</button><br>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <a class="card-header collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
                                                        Products
                                                        <i class="fa fa-lg fa-angle-up pull-right"></i>
                                                    </a>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                                        <div class="card-body">
                                                            <form class="form-group">
                                                                <input class="form-control mt-2 title" name="title" placeholder="Menu Title" type="text" id="title" required>
                                                                <select class="form-control mt-1 url" name="url">
                                                                    @foreach($products as $product)
                                                                        <option value="/product-details/{{ $product->id }}">{{ $product->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select class="form-control new-menu-item mt-1 target" name="target" required>
                                                                    <option value="_self">Current Tab</option>
                                                                    <option value="_blank">New Tab</option>
                                                                </select>
                                                                <input type="file" name="menu_image" class="mt-2 menu_image">
                                                                <button class="btn btn-primary mt-2 mb-2 pull-right addItem" type="button" onclick="addItems(this)">Add To Menu</button><br>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <a class="card-header collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
                                                        Product Categories
                                                        <i class="fa fa-lg fa-angle-up pull-right"></i>
                                                    </a>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                                                        <div class="card-body">
                                                            <form class="form-group">
                                                                <input class="form-control mt-2 title" name="title" placeholder="Menu Title" type="text" id="title" required>
                                                                <select class="form-control mt-1 url" name="url">
                                                                    @foreach($product_categories as $product_category)
                                                                        <option value="/category-wise-products/{{ strtolower(str_replace(' ','-',$product_category->name)) }}">{{ $product_category->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select class="form-control new-menu-item mt-1 target" name="target" required>
                                                                    <option value="_self">Current Tab</option>
                                                                    <option value="_blank">New Tab</option>
                                                                </select>
                                                                <input type="file" name="menu_image" class="mt-2 menu_image">
                                                                <button class="btn btn-primary mt-2 mb-2 pull-right addItem" type="button" onclick="addItems(this)">Add To Menu</button><br>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <a class="card-header collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false">
                                                        Product Subcategories
                                                        <i class="fa fa-lg fa-angle-up pull-right"></i>
                                                    </a>
                                                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion" style="">
                                                        <div class="card-body">
                                                            <form class="form-group">
                                                                <input class="form-control mt-2 title" name="title" placeholder="Menu Title" type="text" id="title" required>
                                                                <select class="form-control mt-1 pro_category" name="category" id="pro_category">
                                                                    <option value="">--Select Category --</option>
                                                                    @foreach($product_categories as $product_category)
                                                                        <option value="{{ $product_category->id }}">{{ $product_category->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <select class="form-control mt-1 pro_subcategory url" name="url" >

                                                                </select>
                                                                <select class="form-control new-menu-item mt-1 target" name="target" required>
                                                                    <option value="_self">Current Tab</option>
                                                                    <option value="_blank">New Tab</option>
                                                                </select>
                                                                <input type="file" name="menu_image" class="mt-2 menu_image">
                                                                <button class="btn btn-primary mt-2 mb-2 pull-right addItem" type="button" onclick="addItems(this)">Add To Menu</button><br>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <a class="card-header collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false">
                                                        Custom Link
                                                        <i class="fa fa-lg fa-angle-up pull-right"></i>
                                                    </a>
                                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion" style="">
                                                        <div class="card-body">
                                                            <form class="form-group">
                                                                <input class="form-control mt-2 title" name="title" placeholder="Menu Title" type="text" id="title" required>
                                                                <input class="form-control mt-1 url" name="url" placeholder="Custom URL" type="text" required>
                                                                <select class="form-control new-menu-item mt-1 target" name="target" required>
                                                                    <option value="_self">Current Tab</option>
                                                                    <option value="_blank">New Tab</option>
                                                                </select>
                                                                <input type="file" name="menu_image" class="mt-2 menu_image">
                                                                <button class="btn btn-primary mt-2 mb-2 pull-right addItem" type="button" onclick="addItems(this)">Add To Menu</button><br>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <form class="form-horizontal edit-menu" style="display: none">
                                                <fieldset>
                                                    <legend>Edit Menu Item</legend>
                                                    <div class="form-group mb-0">
                                                        <label class="col-md-3 control-label text-left mb-2">Title</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" name="title" placeholder="Menu Title" type="text" id="updateTitle" required>
                                                            <p class="note update-menu-title-error" style="display: none; color: red"><strong>Note:</strong> Menu Title Required</p>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="updateId" name="id">

                                                    <div class="form-group mb-0">
                                                        <label class="col-md-3 control-label text-left mb-2" for="updatePageTarget">Page Target</label>
                                                        <div class="col-md-9">
                                                            <select class="form-control" id="updatePageTarget" required>
                                                                <option value="_self">Current Tab</option>
                                                                <option value="_blank">New Tab</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-0">
                                                        <label class="col-md-3 control-label text-left mb-2" for="updatePageTarget">Menu Image</label>
                                                        <div class="col-md-9">
                                                            <div class="box-body">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="max-width: 150px; max-height: 150px;">
                                                                        <img src="http://placehold.it/150X150" width="100%" alt="menu image" id="menu_image">
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"></div>
                                                                    <div>
                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                                                                            <input type="file" name="menu_image" id="menu_image_file">
                                                                        </span>
                                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" id="remove_image">Remove</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <button class="btn btn-primary btn-sm pull-right" type="submit" id="updateMenu">
                                                                Update
                                                            </button>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                            </form>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

        </section>
    </div>
@endsection

@section('script')
    {{-- Jquery Toster--}}
    <script src="/ic_admin/js/jquery.toaster.js"></script>
    <script>
        function addItems(e){
            var title=$(e).parent().find('.title').val();
            var url=$(e).parent().find('.url').val();
            var target=$(e).parent().find('.target').val();
            var menu_image=$(e).parent().find('.menu_image')[0].files[0];
            var formData= new FormData();
            if (menu_image == undefined){
                menu_image=null;
            }
            else {
                formData.append("menu_image", menu_image);
            }
            formData.append("title", title);
            formData.append("url", url);
            formData.append("target", target);
            formData.append("_token", '{{ csrf_token() }}');

            var parent_id=null;

            if (title == ''){
                $('.menu-title-error').show();
                return false;
            }
            else {
                $('.menu-title-error').hide();
            }

            $.ajax({
                url: "/admin/menu/create",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    $('#nestable').load('/admin/menu/load_data');
                    $('.title').val('');
                    $('.menu_image').val('');
                    $.toaster('Menu Added Successfully', 'Success', 'success');
                }
            });
        };

        $(document).ready(function() {
            // PAGE RELATED SCRIPTS

            // activate Nestable for list 1
            $('#nestable').nestable({
                group : 1
            });

            $('.dd').on('change', function (e) {
                $.post('/admin/menu/order', {
                    order: JSON.stringify($('.dd').nestable('serialize')),
                    _token: '{{ csrf_token() }}'
                }, function (data) {
                    $.toaster('Menu Order Updated Successfully', 'Success', 'success');
                });
            });


            $('#nestable-menu').on('click', function(e) {
                var target = $(e.target), action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });


            $('.pro_category').change(function () {
                var pro_category=$('#pro_category').val();
                $(".pro_subcategory").load("/get-subcategories/"+pro_category);
            });

            $('#addMenu').click(function (event) {
                event.preventDefault();
                var title=$('#title').val();
                var url=$('#url').val();
                var parent_id=$('#parent').val();
                var target=$('#target').val();

                if (title == ''){
                    $('.menu-title-error').show();
                    return false;
                }
                else {
                    $('.menu-title-error').hide();
                }

                $.post('/admin/menu/create', {
                    title: title,
                    url: url,
                    target: target,
                    parent_id: parent_id,
                    _token: '{{ csrf_token() }}'
                },function (data) {
                    $('#nestable').load('/admin/menu/load_data');
                    $('.new-menu-item').val('');
                    $.toaster('Menu Updated Successfully', 'Success', 'success');
                });
            });

            $(document).on('click', '#updateMenu', function (event) {
                event.preventDefault();
                var id=$('#updateId').val();
                var title=$('#updateTitle').val();
                var url=$('#updateUrl').val();
                var target=$('#updatePageTarget').val();
                if (title == ''){
                    $('.update-menu-title-error').show();
                    return false;
                }
                else {
                    $('.menu-title-error').hide();
                }
                var menu_image=$('#menu_image_file')[0].files[0];
                var formData= new FormData();
                if (menu_image == undefined){
                    menu_image=null;
                }
                else {
                    formData.append("menu_image", menu_image);
                }
                formData.append("id", id);
                formData.append("title", title);
                formData.append("target", target);
                formData.append("_token", '{{ csrf_token() }}');


                $.ajax({
                    url: "/admin/menu/update",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        $('#nestable').load('/admin/menu/load_data');
                        $.toaster('Menu Updated Successfully', 'Success', 'success');
                    }
                });
            });

            $(document).on('click','.button-delete', function () {
                if (confirm('Are You Sure ?')) {
                    var targetId = $(this).data('id');
                    $.get('/admin/menu/delete/'+targetId,function (data) {
                        $('#nestable').load('/admin/menu/load_data');
                    });

                    $.toaster('Menu Deleated Successfully', 'Success', 'success');
                }
            });

            $(document).on('click', '.add-menu-items', function () {
                $('.add-menu').show();
                $('.edit-menu').hide();
            });
            $(document).on('click','.button-edit', function () {
                var targetId = $(this).data('id');
                var targetTitle = $(this).data('title');
                var targetUrl = $(this).data('url');
                var pageTarget = $(this).data('target');
                var menuImage = $(this).data('image');

                $('#updateId').val(targetId);
                $('#updateTitle').val(targetTitle);
                $('#updateUrl').val(targetUrl);
                $('#updatePageTarget').val(pageTarget);
                $('#remove_image').click();
                if (menuImage.length){
                    $('#menu_image').attr('src','/storage/' + menuImage);
                }
                else {
                    $('#menu_image').attr('src','http://placehold.it/150X150');
                }

                $('.add-menu').hide();
                $('.edit-menu').show();
            });
        });

        $('.note').delay(3000).fadeOut(350);
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
