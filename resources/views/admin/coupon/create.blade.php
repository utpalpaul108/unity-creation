@extends('admin.layout.app')

@section('page_title','Admin | Create new coupon')

@section('style')
    <link rel="stylesheet" href="/ic_admin/jasny-bootstrap/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="/ic_admin/css/forms.css">
@endsection

@section('contents')
    <ol class="align-items-center sa-page-ribbon breadcrumb" aria-label="breadcrumb" role="navigation">
        <li><span id="refresh" class="btn sa-ribbon-btn sa-theme-btn" data-action="resetWidgets"><i class="fa fa-refresh"></i></span></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\DashboardController@index')}})">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ action('Admin\CouponController@index') }}">coupons</a></li>
    </ol>

    <!-- END .sa-page-breadcrumb -->

    <div class="sa-content">
        <div class="d-flex w-100 home-header">
            <div>
                <h1 class="page-header"><i class="fa-fw fa fa-home"></i> Dashboard <span>> Create new coupon</span></h1>
            </div>
        </div>


        <div class="w-100">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <form id="sliderGroup" method="post" action="{{ action('Admin\CouponController@store') }}">
                    @csrf
                    <!-- row -->
                    <div class="row">

                        <!-- NEW WIDGET ROW START -->
                        <div class="col-md-9">
                            @include('flash::message')
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"

                                -->
                                <header>
                                    <div class="widget-header">
                                        <h2>Crete Coupon</h2>
                                    </div>
                                </header>

                                <!-- widget div-->

                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <fieldset>
                                            <legend>
                                                Form Data
                                            </legend>
                                            <div class="form-group">
                                                <label>Coupon Code</label>
                                                <input type="text" class="form-control" name="coupon_code" placeholder="Coupon Code" value="{{ old('coupon_code') }}" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control" name="description" rows="4" placeholder="Description">{{ old('description') }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Discount Type</label>
                                                <select class="form-control" name="discount_type">
                                                    <option value="fixed">Fixed Cart Discount</option>
                                                    <option value="percentage">Percentage Discount</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Discount Amount</label>
                                                <input type="number" class="form-control" name="amount" placeholder="Discount Amount" value="{{ old('amount') }}" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Expiry Date:</label>
                                                <div class="input-group">
                                                    <input type="text" name="expiry_date" value="{{ old('expiry_date') }}" placeholder="Select expiry date" class="form-control datepicker" data-dateformat="yy-mm-dd" required>
                                                    <span class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Usage limit per coupon</label>
                                                <input type="number" class="form-control" name="limit_per_coupon" placeholder="Limit per coupon" value="{{ old('limit_per_coupon') }}" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Usage limit per user</label>
                                                <input type="number" class="form-control" name="limit_per_user" placeholder="Limit per user" value="{{ old('limit_per_user') }}" required />
                                            </div>
                                        </fieldset>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button class="btn btn-default" type="submit">
                                                        <i class="fa fa-send"></i>
                                                        Create
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->

                        </div>
                        <!-- WIDGET ROW END -->

                    </div>
                </form>
                <!-- end row -->
            </section>
            <!-- end widget grid -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

            // form validation

            $('#sliderGroup').bootstrapValidator({
                feedbackIcons : {
                    valid : 'fa fa-check',
                    invalid : 'fa fa-times',
                    validating : 'fa fa-refresh'
                },
                fields : {
                    coupon_code : {
                        validators : {
                            notEmpty : {
                                message : 'Coupon code is required'
                            },
                        }
                    },
                    amount : {
                        validators : {
                            notEmpty : {
                                message : 'Discount amount is required'
                            },
                        }
                    },
                    expiry_date : {
                        validators : {
                            notEmpty : {
                                message : 'Expiry date is required'
                            },
                        }
                    },
                    limit_per_coupon : {
                        validators : {
                            notEmpty : {
                                message : 'Limit per coupon is required'
                            },
                        }
                    },
                    limit_per_user : {
                        validators : {
                            notEmpty : {
                                message : 'Limit per user is required'
                            },
                        }
                    }
                }
            });

            // end profile form

            $('div.alert').delay(3000).fadeOut(350);

        })
    </script>
    <script src="/ic_admin/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
@endsection
