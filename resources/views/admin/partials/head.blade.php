<head>
    <title>@yield('page_title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,500,700">
    <link rel="shortcut icon" href="/images/favicon/favicon.png" type="image/x-icon">
    <link rel="icon" href="/images/favicon/favicon.png" type="image/x-icon">
    <link rel="stylesheet" media="screen, print" href="/ic_admin/css/vendors.bundle.css">
    <link rel="stylesheet" media="screen, print" href="/ic_admin/css/app.bundle.css">
    <link rel="stylesheet" type="text/css" href="/ic_admin/css/datatables.css">

    @yield('style')
</head>
