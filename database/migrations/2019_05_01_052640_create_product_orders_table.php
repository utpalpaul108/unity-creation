<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id')->unique()->nullable();
            $table->unsignedBigInteger('order_by');
            $table->foreign('order_by')->references('id')->on('users')->onDelete('cascade');
            $table->text('billing_address')->nullable();
            $table->text('shipping_address')->nullable();
            $table->string('order_status');
            $table->timestamp('delivery_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_orders');
    }
}
