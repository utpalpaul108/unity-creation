//**BACK-TO-TOP-JS
// fade in #back-top
$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-top').fadeIn();
        } else {
            $('.back-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('.back-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 1600);
        return false;
    });
});


$('.btn-toggle').click(function(){
	$(this).toggleClass('open');
	$('#navbarMob').slideToggle();
	$('#user-nav').removeClass('show');
});

$('.user-btn').click(function(){
	$('#navBar').removeClass('show');
	$('.btn-toggle').removeClass('open');
});

$('.search-tog').click(function(){
	$(this).toggleClass('show-search');
	$('.search-tp').slideToggle();
	
});


